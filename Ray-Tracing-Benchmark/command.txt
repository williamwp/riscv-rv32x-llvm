/home/u/gitlab-wp/phd1/pin/pin -t /home/u/gitlab-wp/phd1/pin/source/tools/MICA-ILP/obj-intel64/mica.so -- qemu-mips-static 7-mips 

mips-linux-gnu-g++-9  

xtensa-lx106-elf-g++ 

x86_64-linux-gnu-g++-9

sparc64-linux-gnu-cpp-9

riscv64-unknown-elf-g++

riscv32-unknown-elf-g++

powerpc-linux-gnu-c++filt

powerpc64-linux-gnu-cpp-9

powerpc64le-linux-gnu-gcc-9

aarch64-linux-gnu-c++filt

aarch64-linux-gnu-gcc-9

 aarch64-linux-gnu-c++filt

aarch64-linux-gnu-cpp-9

 arm-linux-gnueabi-c++filt  

arm-linux-gnueabi-cpp-9

xtensa-lx106-elf-g++

xtensa-lx106-elf-c++

xtensa-lx106-elf-cpp

# SUPPORT MIPS + ARM + 

// 1  OK -- arm (32 bit)

u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ arm-linux-gnueabihf-g++ -static  1-rendering-background.cpp 
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ qemu-arm a.out 
Scanlines remaining: 0   
Done.
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ 



// 2  OK -- aarch64

u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ aarch64-linux-gnu-g++-9 -static  7-metal-reflection.cpp  camera.cpp   color.cpp  hittable_list.cpp  material.cpp  ray.cpp  sphere.cpp  vec3.cpp -o 7-aarch64
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ qemu-aarch64-static
qemu: no user program specified
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ qemu-aarch64-static 7-aarch64 
Scanlines remaining: 219 


// 3  OK -- mips

u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ mips-linux-gnu-g++-9 -static  1-rendering-background.cpp vec3.cpp color.cpp 
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ qemu-mips-static ./a.out 
Scanlines remaining: 0   
Done.


// 4  OK -- alpha  

u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ alpha-linux-gnu-g++-9 -static  7-metal-reflection.cpp  camera.cpp   color.cpp  hittable_list.cpp  material.cpp  ray.cpp  sphere.cpp  vec3.cpp -o aaa
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ qemu-alpha-static ./aaa 
Scanlines remaining: 0   
Done.


// 5  OK  -- powerpc  
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ powerpc-linux-gnu-g++-9 -static  7-metal-reflection.cpp  camera.cpp   color.cpp  hittable_list.cpp  material.cpp  ray.cpp  sphere.cpp  vec3.cpp -o 7-powerpc
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ qemu-ppc-static  ./7-powerpc 
Scanlines remaining: 208 



// 6  OK  -- powerpc64 (ppc64) faster than ppc 32 above. 

u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ powerpc64-linux-gnu-g++-9 -static  7-metal-reflection.cpp  camera.cpp   color.cpp  hittable_list.cpp  material.cpp  ray.cpp  sphere.cpp  vec3.cpp -o 7-ppc64
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ qemu-ppc64-static 7-
7-aarch64               7-metal-reflection.cpp  7-ppc64
7-alpha                 7-mips                  
7-arm                   7-powerpc               
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ qemu-ppc64-static 7-ppc64 
Scanlines remaining: 0   
Done.


//  7  OK  -- hppa 

u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ hppa-linux-gnu-g++-9 -static  7-metal-reflection.cpp  camera.cpp   color.cpp  hittable_list.cpp  material.cpp  ray.cpp  sphere.cpp  vec3.cpp -o 7-hppa
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ qemu-hppa-static  ./7-hppa 
Scanlines remaining: 0   
Done.


//  8   OK  --  m68k  fast
  @u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ m68k-linux-gnu-g++-9 -static  7-metal-reflection.cpp  camera.cpp   color.cpp  hittable_list.cpp  material.cpp  ray.cpp  sphere.cpp  vec3.cpp -o 7-m68k
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ qemu-m68k-static 7-m
7-m68k                  7-metal-reflection.cpp  7-mips                  
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ qemu-m68k-static 7-m
7-m68k                  7-metal-reflection.cpp  7-mips                  
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ qemu-m68k-static 7-m
7-m68k                  7-metal-reflection.cpp  7-mips                  
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ qemu-m68k-static 7-m68k 
Scanlines remaining: 0   
Done.



//  9   OK   -- riscv64 fast

u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ riscv64-linux-gnu-g++-9 -static  7-metal-reflection.cpp  camera.cpp   color.cpp  hittable_list.cpp  material.cpp  ray.cpp  sphere.cpp  vec3.cpp -o 7-riscv64
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ qemu-riscv64-static  ./7-riscv64 
Scanlines remaining: 0   
Done.




//  10  OK  -- s390x   fast

u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ s390x-linux-gnu-g++-9 -static  7-metal-reflection.cpp  camera.cpp   color.cpp  hittable_list.cpp  material.cpp  ray.cpp  sphere.cpp  vec3.cpp -o 7-s390x
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ qemu-s390x-static 7-s390x 
Scanlines remaining: 0   
Done.


//  11   -- OK    sh4  little fast

u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ sh4-linux-gnu-g++-9 -static  7-metal-reflection.cpp  camera.cpp   color.cpp  hittable_list.cpp  material.cpp  ray.cpp  sphere.cpp  vec3.cpp -o 7-sh4
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ qemu-sh4-static 7-sh4 
Scanlines remaining: 0   
Done.



//  12  OK  --  sparc64   slow

u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ sparc64-linux-gnu-g++-9 -static  7-metal-reflection.cpp  camera.cpp   color.cpp  hittable_list.cpp  material.cpp  ray.cpp  sphere.cpp  vec3.cpp -o 7-sparc64
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ qemu-sparc64-static  7-sparc64 
Scanlines remaining: 0   
Done.



//  13  OK  --  mipsel  so so fast
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ mipsel-linux-gnu-g++-9 -static  7-metal-reflection.cpp  camera.cpp   color.cpp  hittable_list.cpp  material.cpp  ray.cpp  sphere.cpp  vec3.cpp -o 7-mipsel
u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ qemu-mipsel-static 7-mipsel 
Scanlines remaining: 0   
Done.


//  14  --  LoongArch





/home/u/gitlab-wp/phd1/pin/pin -t /home/u/gitlab-wp/phd1/pin/source/tools/MICA-ILP/obj-intel64/mica.so -- qemu-mips-static 7-mips

-static  7-metal-reflection.cpp  camera.cpp   color.cpp  hittable_list.cpp  material.cpp  ray.cpp  sphere.cpp  vec3.cpp -o 

u@u-virtual-machine:~/gitlab-wp$ ls /usr/bin/qemu-*-static
/usr/bin/qemu-aarch64_be-static    /usr/bin/qemu-or1k-static
/usr/bin/qemu-aarch64-static       /usr/bin/qemu-ppc64abi32-static
/usr/bin/qemu-alpha-static         /usr/bin/qemu-ppc64le-static
/usr/bin/qemu-armeb-static         /usr/bin/qemu-ppc64-static
/usr/bin/qemu-arm-static           /usr/bin/qemu-ppc-static
/usr/bin/qemu-cris-static          /usr/bin/qemu-riscv32-static
/usr/bin/qemu-hppa-static          /usr/bin/qemu-riscv64-static
/usr/bin/qemu-i386-static          /usr/bin/qemu-s390x-static
/usr/bin/qemu-m68k-static          /usr/bin/qemu-sh4eb-static
/usr/bin/qemu-microblazeel-static  /usr/bin/qemu-sh4-static
/usr/bin/qemu-microblaze-static    /usr/bin/qemu-sparc32plus-static
/usr/bin/qemu-mips64el-static      /usr/bin/qemu-sparc64-static
/usr/bin/qemu-mips64-static        /usr/bin/qemu-sparc-static
/usr/bin/qemu-mipsel-static        /usr/bin/qemu-tilegx-static
/usr/bin/qemu-mipsn32el-static     /usr/bin/qemu-x86_64-static
/usr/bin/qemu-mipsn32-static       /usr/bin/qemu-xtensaeb-static
/usr/bin/qemu-mips-static          /usr/bin/qemu-xtensa-static
/usr/bin/qemu-nios2-static
u@u-virtual-machine:~/gitlab-wp$

sudo apt install g++-9-powerpc64-linux-gnu  

u@u-virtual-machine:~/gitlab-wp/riscv-rv32x-llvm/Ray-Tracing-Benchmark$ sudo apt install g++-9-s390x-linux-gnu


cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE:STRING=Release -DLLVM_ENABLE_PROJECTS="lld;clang" -DCMAKE_INSTALL_PREFIX:PATH=<install_dir> -DLLVM_TARGETS_TO_BUILD:STRING=LoongArch -DCMAKE_C_COMPILER:FILEPATH=<CROSS_COMPILE_PREFIX>gcc -DCMAKE_CXX_COMPILER:FILEPATH=<CROSS_COMPILE_PREFIX>g++ ..

cmake -DLLVM_TARGETS_TO_BUILD="X86;RISCV;LoongArch" -G "Unix Makefiles" ..


